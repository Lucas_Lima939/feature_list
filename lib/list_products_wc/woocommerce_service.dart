import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:testing_list_feature/freightage/models/order_models.dart/order_model.dart';
import 'package:testing_list_feature/list_products_wc/models/attribute_model.dart';

import 'models/attribute_terms_model.dart';

class WoocommerceService {
  static WoocommerceService _instance;

  factory WoocommerceService() => _instance ??= new WoocommerceService._();

  final BASE_URL = 'https://devapp.likebag.com.br/wp-json/wc/v3/';
  final CONSUMER_KEY_SECRET =
      'consumer_key=ck_dbfc4869adeaea59593cd82dc3b004f206fb7589&consumer_secret=cs_b8dfa76f6b1c22c3c369b294b429ba6ed68457f8';

  WoocommerceService._();

  Future<List<Map>> getProductsPerPage(int perPage, int page) async {
    final Map<String, String> headers = {
      'per_page': '$perPage',
      'page': '$page',
      'orderby': 'date',
      'order': 'desc',
    };
    final url = BASE_URL + "products?" + CONSUMER_KEY_SECRET;
    final http.Response res = await http.get(url, headers: headers);
    if (res.statusCode == 200) {
      final List jsonList = jsonDecode(res.body);
      return jsonList.map((dyn) {
        final Map map = dyn;
        return map;
      }).toList();
    } else {
      return [];
    }
  }

  Future<List<Map>> getProductsSearch(String filter) async {
    final url = BASE_URL + "products?search=$filter&" + CONSUMER_KEY_SECRET;
    final http.Response res = await http.get(url);
    if (res.statusCode == 200) {
      final List jsonList = jsonDecode(res.body);
      return jsonList.map((dyn) {
        final Map map = dyn;
        return map;
      }).toList();
    } else {
      return [];
    }
  }

  Future<List<Map>> getProductsFiltered(String slug, int attributeTerm) async {
    final url = BASE_URL +
        "products?attribute=$slug&attribute_term=$attributeTerm&" +
        CONSUMER_KEY_SECRET;
    final http.Response res = await http.get(url);
    if (res.statusCode == 200) {
      final List jsonList = jsonDecode(res.body);
      return jsonList.map((dyn) {
        final Map map = dyn;
        return map;
      }).toList();
    } else {
      return [];
    }
  }

  Future<List<Map>> getAllCategories() async {
    final url = BASE_URL + "products/categories?" + CONSUMER_KEY_SECRET;
    final http.Response res = await http.get(url);
    if (res.statusCode == 200) {
      final List jsonList = jsonDecode(res.body);
      return jsonList.map((dyn) {
        final Map map = dyn;
        return map;
      }).toList();
    } else {
      return [];
    }
  }

  Future<List<AttributeModel>> getAllAtributes() async {
    final url = BASE_URL + "products/attributes?" + CONSUMER_KEY_SECRET;
    final http.Response res = await http.get(url);
    if (res.statusCode == 200) {
      final List jsonList = jsonDecode(res.body);
      return jsonList.map((json) => AttributeModel.fromJson(json)).toList();
    } else {
      return [];
    }
  }

  Future<List<AttributeTerms>> getAllAtributesTerms(int attributeId) async {
    final url = BASE_URL +
        "products/attributes/$attributeId/terms?" +
        CONSUMER_KEY_SECRET;
    final http.Response res = await http.get(url);
    if (res.statusCode == 200) {
      final List jsonList = jsonDecode(res.body);
      return jsonList.map((json) => AttributeTerms.fromJson(json)).toList();
    } else {
      return [];
    }
  }

  Future<Order> getOrders() async {
    final url = BASE_URL + "orders?" + CONSUMER_KEY_SECRET;
    final http.Response res = await http.get(url);
    if (res.statusCode == 200)
      return Order.fromJson(jsonDecode(res.body).first);
    else
      return null;
  }

  Future<List<Order>> getOrdersProcessing(int page) async {
    List<Order> _orders = [];
    final int order_per_page = 100;
    final url = BASE_URL +
        "orders?status=processing&per_page=$order_per_page&page=$page&" +
        CONSUMER_KEY_SECRET;
    final http.Response res = await http.get(url);
    if (res.statusCode == 200) {
      jsonDecode(res.body).forEach((x) => _orders.add(Order.fromJson(x)));
      return _orders;
    } else {
      return [];
    }
  }
}
