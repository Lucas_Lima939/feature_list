import 'package:flutter/material.dart';
import 'package:testing_list_feature/list_products_wc/products_bloc.dart';
import 'package:testing_list_feature/list_products_wc/products_cus_shimmer.dart';
import 'models/product_model.dart';
import 'widgets/likebag_theme_app.dart';
import 'widgets/product_card.dart';

class DataSearch extends SearchDelegate<String> {
  ProductsBloc productsBloc = ProductsBloc();

  @override
  String get searchFieldLabel => 'Pesquisa';

  @override
  List<Widget> buildActions(BuildContext context) {
    return [IconButton(icon: Icon(Icons.clear), onPressed: () {})];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.arrow_back_ios),
        onPressed: () => Navigator.pop(context));
  }

  @override
  Widget buildResults(BuildContext context) {
    productsBloc.productsSearch(query);
    return StreamBuilder<List<Product>>(
      stream: productsBloc.outSearch,
      builder: (_context, _snapshot) {
        if (!_snapshot.hasData) {
          return ProductsCusShimmer();
        } else if (!productsBloc.loadingProducts &&
            _snapshot.data.length == 0) {
          return Center(
            child: Text('A pesquisa não retornou resultados'),
          );
        } else {
          return ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 4.0),
            itemExtent: 125,
            itemCount: _snapshot.data.length,
            itemBuilder: (_context, index) {
              return ProductCard(
                product: _snapshot.data[index],
              );
            },
          );
        }
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return StreamBuilder<List<Product>>(
      stream: productsBloc.outProducts,
      builder: (_context, _snapshot) {
        if (!_snapshot.hasData) {
          return ProductsCusShimmer();
        } else {
          return Column(children: [
            Container(
              height: 35,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.fromLTRB(3, 3, 15, 3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                      icon: Icon(Icons.format_list_numbered),
                      color: LikeBagThemeApp.blue,
                      onPressed: () {}),
                  Text(
                    "Ordernar",
                    style: TextStyle(
                      color: LikeBagThemeApp.blue,
                    ),
                  ),
                  IconButton(
                      icon: Icon(Icons.filter_list),
                      color: LikeBagThemeApp.blue,
                      onPressed: () {}),
                  Text(
                    "Filtrar",
                    style: TextStyle(
                      color: LikeBagThemeApp.blue,
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              height: 5,
              color: LikeBagThemeApp.blue,
            ),
            Flexible(
              child: ListView.builder(
                itemExtent: 125,
                itemCount: _snapshot.data.length +
                    1, // +1 é o espaço para a mensagem de reload
                itemBuilder: (_context, index) {
                  if (_snapshot.data.length - index == 15) {
                    productsBloc.loadMoreProducts();
                  }
                  if (index < _snapshot.data.length) {
                    return ProductCard(
                      product: _snapshot.data[index],
                    );
                  } else if (productsBloc.hasMore) {
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 32.0),
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  } else {
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 32.0),
                      child: Center(
                        child: Text('Nada Mais para Carregar'),
                      ),
                    );
                  }
                },
              ),
            ),
          ]);
        }
      },
    );
  }
}
