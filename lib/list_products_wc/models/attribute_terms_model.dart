import 'package:json_annotation/json_annotation.dart';
import 'package:testing_list_feature/list_products_wc/models/product_links_model.dart';

part 'attribute_terms_model.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class AttributeTerms {
  int id;
  String name;
  String slug;
  String description;
  int menuOrder;
  int count;
  Links lLinks;

  AttributeTerms(
      {this.id,
        this.name,
        this.slug,
        this.description,
        this.menuOrder,
        this.count,
        this.lLinks});

  factory AttributeTerms.fromJson(Map<String, dynamic> json) => _$AttributeTermsFromJson(json);

  Map<String, dynamic> toJson() => _$AttributeTermsToJson(this);
}