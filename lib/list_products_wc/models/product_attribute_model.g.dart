// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_attribute_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AttributeProduct _$AttributeProductFromJson(Map<String, dynamic> json) {
  return AttributeProduct(
    id: json['id'] as int,
    name: json['name'] as String,
    position: json['position'] as int,
    visible: json['visible'] as bool,
    variation: json['variation'] as bool,
    options: (json['options'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$AttributeProductToJson(AttributeProduct instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('name', instance.name);
  writeNotNull('position', instance.position);
  writeNotNull('visible', instance.visible);
  writeNotNull('variation', instance.variation);
  writeNotNull('options', instance.options);
  return val;
}
