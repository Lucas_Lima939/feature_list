// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'images_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Images _$ImagesFromJson(Map<String, dynamic> json) {
  return Images(
    id: json['id'] as int,
    dateCreated: json['dateCreated'] as String,
    dateCreatedGmt: json['dateCreatedGmt'] as String,
    dateModified: json['dateModified'] as String,
    dateModifiedGmt: json['dateModifiedGmt'] as String,
    src: json['src'] as String,
    name: json['name'] as String,
    alt: json['alt'] as String,
  );
}

Map<String, dynamic> _$ImagesToJson(Images instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('dateCreated', instance.dateCreated);
  writeNotNull('dateCreatedGmt', instance.dateCreatedGmt);
  writeNotNull('dateModified', instance.dateModified);
  writeNotNull('dateModifiedGmt', instance.dateModifiedGmt);
  writeNotNull('src', instance.src);
  writeNotNull('name', instance.name);
  writeNotNull('alt', instance.alt);
  return val;
}
