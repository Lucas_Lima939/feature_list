// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attribute_terms_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AttributeTerms _$AttributeTermsFromJson(Map<String, dynamic> json) {
  return AttributeTerms(
    id: json['id'] as int,
    name: json['name'] as String,
    slug: json['slug'] as String,
    description: json['description'] as String,
    menuOrder: json['menuOrder'] as int,
    count: json['count'] as int,
    lLinks: json['lLinks'] == null
        ? null
        : Links.fromJson(json['lLinks'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AttributeTermsToJson(AttributeTerms instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('name', instance.name);
  writeNotNull('slug', instance.slug);
  writeNotNull('description', instance.description);
  writeNotNull('menuOrder', instance.menuOrder);
  writeNotNull('count', instance.count);
  writeNotNull('lLinks', instance.lLinks);
  return val;
}
