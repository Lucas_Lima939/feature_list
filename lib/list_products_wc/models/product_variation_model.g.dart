// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_variation_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductVariation _$ProductVariationFromJson(Map<String, dynamic> json) {
  return ProductVariation(
    regular_price: json['regular_price'] as String,
    image: json['image'] == null
        ? null
        : Images.fromJson(json['image'] as Map<String, dynamic>),
    attributes: (json['attributes'] as List)
        ?.map((e) => e == null
            ? null
            : ProductVariationAttribute.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    manageStock: json['manageStock'] as bool,
    stockQuantity: json['stockQuantity'] as int,
  );
}

Map<String, dynamic> _$ProductVariationToJson(ProductVariation instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('regular_price', instance.regular_price);
  writeNotNull('image', instance.image);
  writeNotNull('attributes', instance.attributes);
  writeNotNull('manageStock', instance.manageStock);
  writeNotNull('stockQuantity', instance.stockQuantity);
  return val;
}
