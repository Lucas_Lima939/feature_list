// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_variation_attribute_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductVariationAttribute _$ProductVariationAttributeFromJson(
    Map<String, dynamic> json) {
  return ProductVariationAttribute(
    id: json['id'] as int,
    name: json['name'] as String,
    option: json['option'] as String,
    position: json['position'] as int,
    variation: json['variation'] as bool,
    visible: json['visible'] as bool,
  );
}

Map<String, dynamic> _$ProductVariationAttributeToJson(
    ProductVariationAttribute instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('position', instance.position);
  writeNotNull('visible', instance.visible);
  writeNotNull('variation', instance.variation);
  writeNotNull('name', instance.name);
  writeNotNull('option', instance.option);
  return val;
}
