import 'package:flutter/material.dart';
import 'package:testing_list_feature/list_products_wc/models/attribute_model.dart';
import 'package:testing_list_feature/list_products_wc/models/attribute_terms_model.dart';

import '../cus_grid_tile.dart';
import '../products_bloc.dart';

class AttributeExpandedTile extends StatelessWidget {
  final AttributeModel attribute;
  final ProductsBloc productsBloc;

  const AttributeExpandedTile({Key key, this.attribute, this.productsBloc})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Card(
        child: ExpansionTile(
          title: Text(attribute.name),
          children: <Widget>[
            FutureBuilder(
                future:
                    productsBloc.loadAttributesTerms(attributeId: attribute.id),
                builder:
                    (context, AsyncSnapshot<List<AttributeTerms>> snapshot) {
                  if (!snapshot.hasData)
                    return Container();
                  else
                    return Container(
                      margin: EdgeInsets.symmetric(horizontal: 5),
                      alignment: Alignment.center,
                      child: GridView(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3,
                                      crossAxisSpacing: 10.0,
                                      mainAxisSpacing: 10.0,
                                      childAspectRatio: 1.7),
                          children: snapshot.data
                              .map((attributeTerm) => CusGridTile(onTap: () => productsBloc.loadProductsFiltered(attributeSlug: attribute.slug, attributeTermId: attributeTerm.id), attributeTerm: attributeTerm,))
                              .toList()),
                    );
                })
          ],
        ),
      ),
    );
  }
}
