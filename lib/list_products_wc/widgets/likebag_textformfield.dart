import 'package:flutter/material.dart';
import 'likebag_theme_app.dart';

class LikeBagTextFormField extends StatelessWidget {
  final IconData prefixIcon;
  final IconData sufixIconButton;
  final bool obscure;
  final Function(String) onChanged;
  final Function onPressedIconButton;
  final String Function(String) validator;
  final String labelName;
  final String hintText;
  final TextEditingController controller;
  final int maxLines;
  final TextInputType keyboardType;
  final ShapeBorder border;
  final Stream<String> stream;
  final InputBorder focusedBorder;
  final InputBorder enabledBorder;
  final TextInputAction action;
  final Function onActionTapped;

  LikeBagTextFormField(
      {this.prefixIcon,
      this.sufixIconButton,
      this.obscure,
      this.onChanged,
      this.validator,
      this.labelName,
      this.hintText,
      this.maxLines,
      this.controller,
      this.keyboardType,
      this.border,
      this.onPressedIconButton,
      this.stream,
      this.focusedBorder,
      this.enabledBorder,
      this.action,
      this.onActionTapped});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
        stream: stream,
        builder: (context, snapshot) {
          return TextFormField(
            textInputAction: action,
            onFieldSubmitted: onActionTapped,
            maxLines: maxLines,
            controller: controller,
            validator: validator,
            onChanged: onChanged,
            keyboardType: keyboardType,
            decoration: InputDecoration(
              suffixIcon: IconButton(
                      icon: Icon(sufixIconButton),
                      onPressed: onPressedIconButton ?? () {},
                      color: LikeBagThemeApp.orange) ??
                  null,
              prefixIcon:
                  Icon(prefixIcon, color: LikeBagThemeApp.orange) ?? null,
              focusedBorder: focusedBorder,
              enabledBorder: enabledBorder,
              hintText: hintText,
              labelText: labelName,
              alignLabelWithHint: true,
              border: border == null ? OutlineInputBorder() : border,
            ),
          );
        });
  }
}
