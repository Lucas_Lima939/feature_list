import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LikeBagThemeApp{

  LikeBagThemeApp._();

  static const Color blue = Color(0xFF494375);
  static const Color darkBlue = Color(0xFF010440);
  static const Color offWhite = Color(0xFFF2E3D5);
  static const Color orange = Color(0xFFF24535);
  static const Color red = Color(0xFFF22E2E);


}