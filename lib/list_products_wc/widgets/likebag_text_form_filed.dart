import 'package:flutter/material.dart';
import 'likebag_theme_app.dart';

class LikeBagTextFormField extends StatelessWidget {
  final IconData icon;
  final IconData iconButton;
  final bool obscure;
  final Function(String) onChanged;
  final Function onPressedIconButton;
  final String Function(String) validator;
  final String labelName;
  final String hintText;
  final TextEditingController controller;
  final int maxLines;
  final TextInputType keyboardType;
  final ShapeBorder border;
  final Stream<String> stream;
  final InputBorder focusedBorder;
  final InputBorder enabledBorder;

  LikeBagTextFormField({
    this.icon,
    this.iconButton,
    this.obscure,
    this.onChanged,
    this.validator,
    this.labelName,
    this.hintText,
    this.maxLines,
    this.controller,
    this.keyboardType,
    this.border,
    this.onPressedIconButton,
    this.stream,
    this.focusedBorder,
    this.enabledBorder,
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
        stream: stream,
        builder: (context, snapshot) {
          return TextFormField(
            maxLines: maxLines,
            controller: controller,
            validator: validator,
            onChanged: onChanged,
            keyboardType: keyboardType,
            decoration: InputDecoration(
              focusedBorder: focusedBorder,
              enabledBorder: enabledBorder,
              icon: icon != null
                  ? Icon(icon, color: LikeBagThemeApp.orange)
                  : iconButton != null
                      ? IconButton(
                          icon: Icon(iconButton),
                          onPressed: onPressedIconButton,
                          color: LikeBagThemeApp.orange,
                        )
                      : null,
              hintText: hintText,
              labelText: labelName,
              alignLabelWithHint: true,
              border: border == null ? OutlineInputBorder() : border,
            ),
          );
        });
  }
}
