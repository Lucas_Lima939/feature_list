import 'package:flutter/material.dart';
import 'package:testing_list_feature/list_products_wc/models/attribute_model.dart';
import 'package:testing_list_feature/list_products_wc/products_bloc.dart';
import 'package:testing_list_feature/list_products_wc/widgets/attribute_expanded_tile.dart';
import 'package:testing_list_feature/list_products_wc/widgets/product_card.dart';

import 'models/categories_model.dart';
import 'models/product_model.dart';

class ListFilter extends StatefulWidget {
  @override
  _ListFilterState createState() => _ListFilterState();
}

class _ListFilterState extends State<ListFilter> {
  ProductsBloc _productsBloc = ProductsBloc();

  @override
  void initState() {
    _productsBloc.loadAllAttributes();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(children: [
        StreamBuilder<List<AttributeModel>>(
            stream: _productsBloc.outAttributes,
            builder: (context, snapshot) {
              return !snapshot.hasData
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : ListView(
                      scrollDirection: Axis.vertical,
                      children: snapshot.data
                          .map((attribute) => AttributeExpandedTile(
                                attribute: attribute,
                                productsBloc: _productsBloc,
                              ))
                          .toList(),
                    );
            }),
        Expanded(
          child: StreamBuilder<List<Product>>(
              stream: _productsBloc.outProducts,
              initialData: [],
              builder: (context, AsyncSnapshot<List<Product>> snapshot) {
                if (!snapshot.hasData)
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                else if (snapshot.data.isEmpty)
                  return Center(child: Text("Selecione um Filtro"));
                else
                  return ListView(
                    children: snapshot.data
                        .map((product) => ProductCard(product: product,))
                        .toList(),
                  );
              }),
        ),
      ]),
    );
  }
}
