import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:testing_list_feature/list_products_wc/models/categories_model.dart';
import 'package:testing_list_feature/list_products_wc/woocommerce_service.dart';

import 'models/attribute_model.dart';
import 'models/attribute_terms_model.dart';
import 'models/product_model.dart';

enum ProductsTabState { IDLE, SEARCH }

class ProductsBloc extends BlocBase {
  WoocommerceService service = WoocommerceService();
  bool hasMore;
  int _page = 1;
  bool loadingProducts = false;
  List<Product> _produtos = [];

  final _productsController$ = BehaviorSubject<List<Product>>();
  final _categoriesController$ = BehaviorSubject<List<Categories>>();
  final _attributesController$ = BehaviorSubject<List<AttributeModel>>();
  var _stateController = BehaviorSubject<ProductsTabState>();
  final _searchController$ = BehaviorSubject<List<Product>>();

  Stream get outSearch => _searchController$.stream;
  Stream<List<Product>> get outProducts => _productsController$.stream;
  Stream<List<Categories>> get outCategories => _categoriesController$.stream;
  Stream<List<AttributeModel>> get outAttributes => _attributesController$.stream;
  Stream<ProductsTabState> get outState =>
      _stateController.stream.asBroadcastStream();

  Function(ProductsTabState) get changeStatus => _stateController.sink.add;

  ProductsBloc() {
    hasMore = true;
    refresh();
  }

  Future<void> productsSearch(String search) async {
    if (search.trim().isEmpty) {
      return;
    } else {
      loadingProducts = true;
      _searchController$.add(_filter(search.trim()));
      _filteredProductsFromService(search.trim())
          .then((response) => _searchController$.add(response));
      loadingProducts = false;
    }
  }

  Future<List<Product>> _filteredProductsFromService(String searchValue) async {
    return await service.getProductsSearch(searchValue).then((jsonList) {
      List<Product> responseList =
          jsonList.map((json) => Product.fromJson(json)).toList();
      //filteredList.removeWhere((product) => _produtos.map((produto) => produto.id).toList().contains(product.id));
      return responseList;
    });
  }

  List<Product> _filter(String searchValue) {
    List<Product> filteredProducts = List.from(_produtos);
    filteredProducts.retainWhere((product) {
      return product.name.toUpperCase().contains(searchValue.toUpperCase());
    });
    return filteredProducts;
  }

  Future<void> refresh() async {
    _page = 1;
    _produtos = [];
    await loadMoreProducts();
  }

  Future<void> loadMoreProducts() async {
    await service.getProductsPerPage(20, _page).then((productsMapList) {
      hasMore = (productsMapList.length >= 20);
      _produtos.addAll(productsMapList.map((map) {
        return Product.fromJson(map);
      }).toList());
      _productsController$.add(_produtos);
      _page++;
    });
  }

  Future<void> loadProductsFiltered({@required String attributeSlug, @required int attributeTermId}) async {
    await service
        .getProductsFiltered(attributeSlug, attributeTermId)
        .then((value) => _produtos = value.map((map) {
              return Product.fromJson(map);
            }).toList());
    _productsController$.add(_produtos);
  }

  Future<void> loadAllAttributes() async {
    await service.getAllAtributes().then((attributes) async { 
    _attributesController$.add(attributes);
    for(AttributeModel attribute in attributes){
      attribute.attributeTerms = await service.getAllAtributesTerms(attribute.id);
    }
    _attributesController$.add(attributes);
    });
  }

  Future<List<AttributeTerms>> loadAttributesTerms({@required int attributeId}) async {
    return await service.getAllAtributesTerms(attributeId);
  }

  Future<void> loadAllCategories() async {
    List<Categories> _categories = [];
    await service
        .getAllCategories()
        .then((categoryMap) => _categories.addAll(categoryMap.map((map) {
              return Categories.fromJson(map);
            }).toList()));
    _categoriesController$.add(_categories);
  }


  @override
  void dispose() {
    _stateController.close();
    _productsController$.close();
    _searchController$.close();
    _categoriesController$.close();
    _attributesController$.close();
    super.dispose();
  }
}
