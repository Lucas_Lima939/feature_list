import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'widgets/likebag_theme_app.dart';

class ProductsCusShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  final elementsWidth = MediaQuery.of(context).size.width * 0.45;
  final marginWidth = MediaQuery.of(context).size.width * 0.025;
    return Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      child: ListView.builder(
        itemBuilder: (_, __) => Container(
            foregroundDecoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey,
                width: 0.5,
              ),
              borderRadius: BorderRadius.circular(6),
            ),
            margin: EdgeInsets.only(left: 10, right: 10, top: 5),
            child: Card(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: marginWidth, vertical: 6),
        child: Row(children: [
          Container(
            width: 125,
            height: 125,
            color: LikeBagThemeApp.offWhite,
          ),
          Container(
            margin: EdgeInsets.only(left: 5),
            width: elementsWidth,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    color:  LikeBagThemeApp.offWhite,
                  ),
                  Container(
                    color:  LikeBagThemeApp.offWhite,
                  ),
                  Container(
                    color:  LikeBagThemeApp.offWhite,
                  ),
                ],
              )),
        ]),
      ),
    )
            // ListTile(
            //   leading: CircleAvatar(
            //     backgroundColor: LikeBagThemeApp.offWhite,
            //   ),
            //   title: Container(
            //     width: double.infinity,
            //     height: 8.0,
            //     color: Colors.white,
            //   ),
            //   subtitle: Container(
            //     width: double.infinity,
            //     height: 8.0,
            //     color: Colors.white,
            //   ),
            // )
            ),
        itemCount: 15,
      ),
    );
  }
}
