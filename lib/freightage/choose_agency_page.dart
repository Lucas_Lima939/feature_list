import 'package:flutter/material.dart';
import 'package:testing_list_feature/freightage/cus_grid_tile.dart';
import 'package:testing_list_feature/list_products_wc/widgets/likebag_theme_app.dart';

import 'freightage_bloc.dart';
import 'models/agency_model.dart/agency_model.dart';

class ChooseAgencyPage extends StatefulWidget {
  @override
  _ChooseAgencyPageState createState() => _ChooseAgencyPageState();
}

class _ChooseAgencyPageState extends State<ChooseAgencyPage> {
  FreightageBloc freightageBloc;

  @override
  void initState() {
    freightageBloc = FreightageBloc();
    freightageBloc.listAgencies();    
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Escolha a agência'),
        backgroundColor:  LikeBagThemeApp.darkBlue,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 5),
        child: ListView(
                  children:[ StreamBuilder<List<AgencyModel>>(
            stream: freightageBloc.outAgencies,
            builder: (context, snapshot) {
              if(!snapshot.hasData)
              return Center(child: CircularProgressIndicator(),);
              else
              return GridView(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                        crossAxisSpacing: 10.0,
                                        mainAxisSpacing: 10.0,
                                        childAspectRatio: 1.7),
                            children: snapshot.data
                                .map((agency) => CusGridTile(agency: agency, onTap: () {}))
                                .toList());
            }
          ),]
        ),
      ),
      bottomNavigationBar: BottomAppBar(
          color: LikeBagThemeApp.darkBlue,
          child: Container(
              height: 50.0,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        'AVANÇAR',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {},
                    ),
                  ]))),
    );
  }
}
