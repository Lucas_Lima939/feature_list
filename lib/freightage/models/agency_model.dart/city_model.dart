
import 'state_model.dart';

class City {
  int id;
  String city;
  State state;

  City({this.id, this.city, this.state});

  City.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    city = json['city'];
    state = json['state'] != null ? new State.fromJson(json['state']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['city'] = this.city;
    if (this.state != null) {
      data['state'] = this.state.toJson();
    }
    return data;
  }
}