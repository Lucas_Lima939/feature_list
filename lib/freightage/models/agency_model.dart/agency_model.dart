import 'address_model.dart';
import 'phone_model.dart';

class AgencyModel {
  int id;
  String name;
  String initials;
  String code;
  String companyName;
  String status;
  String email;
  String note;
  int companyId;
  Address address;
  Phone phone;

  AgencyModel(
      {this.id,
      this.name,
      this.initials,
      this.code,
      this.companyName,
      this.status,
      this.email,
      this.note,
      this.companyId,
      this.address,
      this.phone});

  AgencyModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    initials = json['initials'];
    code = json['code'];
    companyName = json['company_name'];
    status = json['status'];
    email = json['email'];
    note = json['note'];
    companyId = json['company_id'];
    address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
    phone = json['phone'] != null ? new Phone.fromJson(json['phone']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['initials'] = this.initials;
    data['code'] = this.code;
    data['company_name'] = this.companyName;
    data['status'] = this.status;
    data['email'] = this.email;
    data['note'] = this.note;
    data['company_id'] = this.companyId;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    if (this.phone != null) {
      data['phone'] = this.phone.toJson();
    }
    return data;
  }
}
