
class Phone {
  int id;
  String label;
  String phone;
  String type;
  String countryId;
  Null confirmedAt;
  String createdAt;
  String updatedAt;

  Phone(
      {this.id,
      this.label,
      this.phone,
      this.type,
      this.countryId,
      this.confirmedAt,
      this.createdAt,
      this.updatedAt});

  Phone.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    label = json['label'];
    phone = json['phone'];
    type = json['type'];
    countryId = json['country_id'];
    confirmedAt = json['confirmed_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['label'] = this.label;
    data['phone'] = this.phone;
    data['type'] = this.type;
    data['country_id'] = this.countryId;
    data['confirmed_at'] = this.confirmedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}