
class Country {
  String id;
  String country;

  Country({this.id, this.country});

  Country.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    country = json['country'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['country'] = this.country;
    return data;
  }}