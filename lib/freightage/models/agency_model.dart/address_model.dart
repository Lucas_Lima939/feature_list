class Address {
  int id;
  String label;
  String postalCode;
  String address;
  String number;
  String complement;
  String district;
  double latitude;
  double longitude;
  String confirmedAt;
  String createdAt;
  String updatedAt;
  City city;

  Address(
      {this.id,
      this.label,
      this.postalCode,
      this.address,
      this.number,
      this.complement,
      this.district,
      this.latitude,
      this.longitude,
      this.confirmedAt,
      this.createdAt,
      this.updatedAt,
      this.city});

  Address.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    label = json['label'];
    postalCode = json['postal_code'];
    address = json['address'];
    number = json['number'];
    complement = json['complement'];
    district = json['district'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    confirmedAt = json['confirmed_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    city = json['city'] != null ? new City.fromJson(json['city']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['label'] = this.label;
    data['postal_code'] = this.postalCode;
    data['address'] = this.address;
    data['number'] = this.number;
    data['complement'] = this.complement;
    data['district'] = this.district;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['confirmed_at'] = this.confirmedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.city != null) {
      data['city'] = this.city.toJson();
    }
    return data;
  }
}

class City {
  int id;
  String city;
  State state;

  City({this.id, this.city, this.state});

  City.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    city = json['city'];
    state = json['state'] != null ? new State.fromJson(json['state']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['city'] = this.city;
    if (this.state != null) {
      data['state'] = this.state.toJson();
    }
    return data;
  }
}

class State {
  int id;
  String state;
  String stateAbbr;
  Country country;

  State({this.id, this.state, this.stateAbbr, this.country});

  State.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    state = json['state'];
    stateAbbr = json['state_abbr'];
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['state'] = this.state;
    data['state_abbr'] = this.stateAbbr;
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    return data;
  }
}

class Country {
  String id;
  String country;

  Country({this.id, this.country});

  Country.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    country = json['country'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['country'] = this.country;
    return data;
  }
}
