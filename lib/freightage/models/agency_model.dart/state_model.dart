

import 'country_model.dart';

class State {
  int id;
  String state;
  String stateAbbr;
  Country country;

  State({this.id, this.state, this.stateAbbr, this.country});

  State.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    state = json['state'];
    stateAbbr = json['state_abbr'];
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['state'] = this.state;
    data['state_abbr'] = this.stateAbbr;
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    return data;
  }
}