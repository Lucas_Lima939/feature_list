// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_billing_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderBilling _$OrderBillingFromJson(Map<String, dynamic> json) {
  return OrderBilling(
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    company: json['company'] as String,
    address1: json['address1'] as String,
    address2: json['address2'] as String,
    city: json['city'] as String,
    state: json['state'] as String,
    postcode: json['postcode'] as String,
    country: json['country'] as String,
    email: json['email'] as String,
    phone: json['phone'] as String,
    number: json['number'] as String,
    neighborhood: json['neighborhood'] as String,
    persontype: json['persontype'] as String,
    cpf: json['cpf'] as String,
    rg: json['rg'] as String,
    cnpj: json['cnpj'] as String,
    ie: json['ie'] as String,
    birthdate: json['birthdate'] as String,
    sex: json['sex'] as String,
    cellphone: json['cellphone'] as String,
  );
}

Map<String, dynamic> _$OrderBillingToJson(OrderBilling instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('firstName', instance.firstName);
  writeNotNull('lastName', instance.lastName);
  writeNotNull('company', instance.company);
  writeNotNull('address1', instance.address1);
  writeNotNull('address2', instance.address2);
  writeNotNull('city', instance.city);
  writeNotNull('state', instance.state);
  writeNotNull('postcode', instance.postcode);
  writeNotNull('country', instance.country);
  writeNotNull('email', instance.email);
  writeNotNull('phone', instance.phone);
  writeNotNull('number', instance.number);
  writeNotNull('neighborhood', instance.neighborhood);
  writeNotNull('persontype', instance.persontype);
  writeNotNull('cpf', instance.cpf);
  writeNotNull('rg', instance.rg);
  writeNotNull('cnpj', instance.cnpj);
  writeNotNull('ie', instance.ie);
  writeNotNull('birthdate', instance.birthdate);
  writeNotNull('sex', instance.sex);
  writeNotNull('cellphone', instance.cellphone);
  return val;
}
