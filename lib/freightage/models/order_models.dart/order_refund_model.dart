class OrderRefund {
  OrderRefund({
    this.id,
    this.reason,
    this.total,
  });

  int id;
  String reason;
  String total;

  factory OrderRefund.fromJson(Map<String, dynamic> json) => OrderRefund(
    id: json["id"],
    reason: json["reason"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "reason": reason,
    "total": total,
  };
}