// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Order _$OrderFromJson(Map<String, dynamic> json) {
  return Order(
    id: json['id'] as int,
    parentId: json['parentId'] as int,
    number: json['number'] as String,
    orderKey: json['orderKey'] as String,
    createdVia: json['createdVia'] as String,
    version: json['version'] as String,
    status: json['status'] as String,
    currency: json['currency'] as String,
    dateCreated: json['dateCreated'] as String,
    dateCreatedGmt: json['dateCreatedGmt'] as String,
    dateModified: json['dateModified'] as String,
    dateModifiedGmt: json['dateModifiedGmt'] as String,
    discountTotal: json['discountTotal'] as String,
    discountTax: json['discountTax'] as String,
    shippingTotal: json['shippingTotal'] as String,
    shippingTax: json['shippingTax'] as String,
    cartTax: json['cartTax'] as String,
    total: json['total'] as String,
    totalTax: json['totalTax'] as String,
    pricesIncludeTax: json['pricesIncludeTax'] as bool,
    customerId: json['customerId'] as int,
    customerIpAddress: json['customerIpAddress'] as String,
    customerUserAgent: json['customerUserAgent'] as String,
    customerNote: json['customerNote'] as String,
    billing: json['billing'] == null
        ? null
        : OrderBilling.fromJson(json['billing'] as Map<String, dynamic>),
    shipping: json['shipping'] == null
        ? null
        : OrderShipping.fromJson(json['shipping'] as Map<String, dynamic>),
    paymentMethod: json['paymentMethod'] as String,
    paymentMethodTitle: json['paymentMethodTitle'] as String,
    transactionId: json['transactionId'] as String,
    datePaid: json['datePaid'] as String,
    datePaidGmt: json['datePaidGmt'] as String,
    dateCompleted: json['dateCompleted'],
    dateCompletedGmt: json['dateCompletedGmt'],
    cartHash: json['cartHash'] as String,
    metaData: (json['metaData'] as List)
        ?.map((e) => e == null
            ? null
            : OrderMetaData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    lineItems: (json['lineItems'] as List)
        ?.map((e) => e == null
            ? null
            : OrderLineItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    taxLines: json['taxLines'] as List,
    shippingLines: (json['shippingLines'] as List)
        ?.map((e) => e == null
            ? null
            : OrderShippingLine.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    feeLines: json['feeLines'] as List,
    couponLines: json['couponLines'] as List,
    refunds: (json['refunds'] as List)
        ?.map((e) =>
            e == null ? null : OrderRefund.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    currencySymbol: json['currencySymbol'] as String,
  );
}

Map<String, dynamic> _$OrderToJson(Order instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('parentId', instance.parentId);
  writeNotNull('number', instance.number);
  writeNotNull('orderKey', instance.orderKey);
  writeNotNull('createdVia', instance.createdVia);
  writeNotNull('version', instance.version);
  writeNotNull('status', instance.status);
  writeNotNull('currency', instance.currency);
  writeNotNull('dateCreated', instance.dateCreated);
  writeNotNull('dateCreatedGmt', instance.dateCreatedGmt);
  writeNotNull('dateModified', instance.dateModified);
  writeNotNull('dateModifiedGmt', instance.dateModifiedGmt);
  writeNotNull('discountTotal', instance.discountTotal);
  writeNotNull('discountTax', instance.discountTax);
  writeNotNull('shippingTotal', instance.shippingTotal);
  writeNotNull('shippingTax', instance.shippingTax);
  writeNotNull('cartTax', instance.cartTax);
  writeNotNull('total', instance.total);
  writeNotNull('totalTax', instance.totalTax);
  writeNotNull('pricesIncludeTax', instance.pricesIncludeTax);
  writeNotNull('customerId', instance.customerId);
  writeNotNull('customerIpAddress', instance.customerIpAddress);
  writeNotNull('customerUserAgent', instance.customerUserAgent);
  writeNotNull('customerNote', instance.customerNote);
  writeNotNull('billing', instance.billing);
  writeNotNull('shipping', instance.shipping);
  writeNotNull('paymentMethod', instance.paymentMethod);
  writeNotNull('paymentMethodTitle', instance.paymentMethodTitle);
  writeNotNull('transactionId', instance.transactionId);
  writeNotNull('datePaid', instance.datePaid);
  writeNotNull('datePaidGmt', instance.datePaidGmt);
  writeNotNull('dateCompleted', instance.dateCompleted);
  writeNotNull('dateCompletedGmt', instance.dateCompletedGmt);
  writeNotNull('cartHash', instance.cartHash);
  writeNotNull('metaData', instance.metaData);
  writeNotNull('lineItems', instance.lineItems);
  writeNotNull('taxLines', instance.taxLines);
  writeNotNull('shippingLines', instance.shippingLines);
  writeNotNull('feeLines', instance.feeLines);
  writeNotNull('couponLines', instance.couponLines);
  writeNotNull('refunds', instance.refunds);
  writeNotNull('currencySymbol', instance.currencySymbol);
  return val;
}
