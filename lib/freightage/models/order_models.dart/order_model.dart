import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

import 'order_billing_model.dart';
import 'order_line_item_model.dart';
import 'order_meta_data_model.dart';
import 'order_refund_model.dart';
import 'order_shipping_line_model.dart';
import 'order_shipping_model.dart';
part 'order_model.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class Order {
  Order({
    this.id,
    this.parentId,
    this.number,
    this.orderKey,
    this.createdVia,
    this.version,
    this.status,
    this.currency,
    this.dateCreated,
    this.dateCreatedGmt,
    this.dateModified,
    this.dateModifiedGmt,
    this.discountTotal,
    this.discountTax,
    this.shippingTotal,
    this.shippingTax,
    this.cartTax,
    this.total,
    this.totalTax,
    this.pricesIncludeTax,
    this.customerId,
    this.customerIpAddress,
    this.customerUserAgent,
    this.customerNote,
    this.billing,
    this.shipping,
    this.paymentMethod,
    this.paymentMethodTitle,
    this.transactionId,
    this.datePaid,
    this.datePaidGmt,
    this.dateCompleted,
    this.dateCompletedGmt,
    this.cartHash,
    this.metaData,
    this.lineItems,
    this.taxLines,
    this.shippingLines,
    this.feeLines,
    this.couponLines,
    this.refunds,
    this.currencySymbol,
  });

  int id;
  int parentId;
  String number;
  String orderKey;
  String createdVia;
  String version;
  String status;
  String currency;
  String dateCreated;
  String dateCreatedGmt;
  String dateModified;
  String dateModifiedGmt;
  String discountTotal;
  String discountTax;
  String shippingTotal;
  String shippingTax;
  String cartTax;
  String total;
  String totalTax;
  bool pricesIncludeTax;
  int customerId;
  String customerIpAddress;
  String customerUserAgent;
  String customerNote;
  OrderBilling billing;
  OrderShipping shipping;
  String paymentMethod;
  String paymentMethodTitle;
  String transactionId;
  String datePaid;
  String datePaidGmt;
  dynamic dateCompleted;
  dynamic dateCompletedGmt;
  String cartHash;
  List<OrderMetaData> metaData;
  List<OrderLineItem> lineItems;
  List<dynamic> taxLines;
  List<OrderShippingLine> shippingLines;
  List<dynamic> feeLines;
  List<dynamic> couponLines;
  List<OrderRefund> refunds;
  String currencySymbol;

  factory Order.fromJson(Map<String, dynamic> json) => Order(
        id: json["id"],
        parentId: json["parent_id"],
        number: json["number"],
        orderKey: json["order_key"],
        createdVia: json["created_via"],
        version: json["version"],
        status: json["status"],
        currency: json["currency"],
        dateCreated: json["date_created"],
        dateCreatedGmt: json["date_created_gmt"],
        dateModified: json["date_modified"],
        dateModifiedGmt: json["date_modified_gmt"],
        discountTotal: json["discount_total"],
        discountTax: json["discount_tax"],
        shippingTotal: json["shipping_total"],
        shippingTax: json["shipping_tax"],
        cartTax: json["cart_tax"],
        total: json["total"],
        totalTax: json["total_tax"],
        pricesIncludeTax: json["prices_include_tax"],
        customerId: json["customer_id"],
        customerIpAddress: json["customer_ip_address"],
        customerUserAgent: json["customer_user_agent"],
        customerNote: json["customer_note"],
        billing: OrderBilling.fromJson(json["billing"]),
        shipping: OrderShipping.fromJson(json["shipping"]),
        paymentMethod: json["payment_method"],
        paymentMethodTitle: json["payment_method_title"],
        transactionId: json["transaction_id"],
        datePaid: json["date_paid"],
        datePaidGmt: json["date_paid_gmt"],
        dateCompleted: json["date_completed"],
        dateCompletedGmt: json["date_completed_gmt"],
        cartHash: json["cart_hash"],
        metaData: List<OrderMetaData>.from(
            json["meta_data"].map((x) => OrderMetaData.fromJson(x))),
        lineItems: List<OrderLineItem>.from(
            json["line_items"].map((x) => OrderLineItem.fromJson(x))),
        taxLines: List<dynamic>.from(json["tax_lines"].map((x) => x)),
        shippingLines: List<OrderShippingLine>.from(
            json["shipping_lines"].map((x) => OrderShippingLine.fromJson(x))),
        feeLines: List<dynamic>.from(json["fee_lines"].map((x) => x)),
        couponLines: List<dynamic>.from(json["coupon_lines"].map((x) => x)),
        refunds: List<OrderRefund>.from(
            json["refunds"].map((x) => OrderRefund.fromJson(x))),
        currencySymbol: json["currency_symbol"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "parent_id": parentId,
        "number": number,
        "order_key": orderKey,
        "created_via": createdVia,
        "version": version,
        "status": status,
        "currency": currency,
        "date_created": dateCreated,
        "date_created_gmt": dateCreatedGmt,
        "date_modified": dateModified,
        "date_modified_gmt": dateModifiedGmt,
        "discount_total": discountTotal,
        "discount_tax": discountTax,
        "shipping_total": shippingTotal,
        "shipping_tax": shippingTax,
        "cart_tax": cartTax,
        "total": total,
        "total_tax": totalTax,
        "prices_include_tax": pricesIncludeTax,
        "customer_id": customerId,
        "customer_ip_address": customerIpAddress,
        "customer_user_agent": customerUserAgent,
        "customer_note": customerNote,
        "billing": billing.toJson(),
        "shipping": shipping.toJson(),
        "payment_method": paymentMethod,
        "payment_method_title": paymentMethodTitle,
        "transaction_id": transactionId,
        "date_paid": datePaid,
        "date_paid_gmt": datePaidGmt,
        "date_completed": dateCompleted,
        "date_completed_gmt": dateCompletedGmt,
        "cart_hash": cartHash,
        "meta_data": List<dynamic>.from(metaData.map((x) => x.toJson())),
        "line_items": List<dynamic>.from(lineItems.map((x) => x.toJson())),
        "tax_lines": List<dynamic>.from(taxLines.map((x) => x)),
        "shipping_lines":
            List<dynamic>.from(shippingLines.map((x) => x.toJson())),
        "fee_lines": List<dynamic>.from(feeLines.map((x) => x)),
        "coupon_lines": List<dynamic>.from(couponLines.map((x) => x)),
        "refunds": List<dynamic>.from(refunds.map((x) => x.toJson())),
        "currency_symbol": currencySymbol,
      };
}
