class OrderMetaData {
  OrderMetaData({
    this.id,
    this.key,
    this.value,
  });

  int id;
  String key;
  dynamic value;

  factory OrderMetaData.fromJson(Map<String, dynamic> json) => OrderMetaData(
    id: json["id"],
    key: json["key"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "key": key,
    "value": value,
  };
}