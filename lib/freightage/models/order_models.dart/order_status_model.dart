import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'order_status_model.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class OrderStatus {
  OrderStatus({
    this.status,
  });

  String status;

  factory OrderStatus.fromJson(Map<String, dynamic> json) => OrderStatus(
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
  };
}
