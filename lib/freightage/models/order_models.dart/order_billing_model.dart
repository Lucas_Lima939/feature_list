import 'package:json_annotation/json_annotation.dart';

part 'order_billing_model.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class OrderBilling {
  OrderBilling({
    this.firstName,
    this.lastName,
    this.company,
    this.address1,
    this.address2,
    this.city,
    this.state,
    this.postcode,
    this.country,
    this.email,
    this.phone,
    this.number,
    this.neighborhood,
    this.persontype,
    this.cpf,
    this.rg,
    this.cnpj,
    this.ie,
    this.birthdate,
    this.sex,
    this.cellphone,
  });

  String firstName;
  String lastName;
  String company;
  String address1;
  String address2;
  String city;
  String state;
  String postcode;
  String country;
  String email;
  String phone;
  String number;
  String neighborhood;
  String persontype;
  String cpf;
  String rg;
  String cnpj;
  String ie;
  String birthdate;
  String sex;
  String cellphone;

  factory OrderBilling.fromJson(Map<String, dynamic> json) => OrderBilling(
    firstName: json["first_name"],
    lastName: json["last_name"],
    company: json["company"],
    address1: json["address_1"],
    address2: json["address_2"],
    city: json["city"],
    state: json["state"],
    postcode: json["postcode"],
    country: json["country"],
    email: json["email"],
    phone: json["phone"],
    number: json["number"],
    neighborhood: json["neighborhood"],
    persontype: json["persontype"],
    cpf: json["cpf"],
    rg: json["rg"],
    cnpj: json["cnpj"],
    ie: json["ie"],
    birthdate: json["birthdate"],
    sex: json["sex"],
    cellphone: json["cellphone"],
  );

  Map<String, dynamic> toJson() => {
    "first_name": firstName,
    "last_name": lastName,
    "company": company,
    "address_1": address1,
    "address_2": address2,
    "city": city,
    "state": state,
    "postcode": postcode,
    "country": country,
    "email": email,
    "phone": phone,
    "number": number,
    "neighborhood": neighborhood,
    "persontype": persontype,
    "cpf": cpf,
    "rg": rg,
    "cnpj": cnpj,
    "ie": ie,
    "birthdate": birthdate,
    "sex": sex,
    "cellphone": cellphone,
  };
}