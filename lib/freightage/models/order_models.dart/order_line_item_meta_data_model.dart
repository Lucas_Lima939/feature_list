class OrderLineItemMetaData {
  OrderLineItemMetaData({
    this.id,
    this.key,
    this.value,
  });

  int id;
  String key;
  dynamic value;

  factory OrderLineItemMetaData.fromJson(Map<String, dynamic> json) => OrderLineItemMetaData(
    id: json["id"],
    key: json["key"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "key": key,
    "value": value,
  };
}