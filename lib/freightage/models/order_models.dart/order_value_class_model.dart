class ValueClass {
  ValueClass({
    this.type,
    this.method,
    this.installments,
    this.link,
  });

  int type;
  String method;
  String installments;
  String link;

  factory ValueClass.fromJson(Map<String, dynamic> json) => ValueClass(
    type: json["type"],
    method: json["method"],
    installments: json["installments"],
    link: json["link"],
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "method": method,
    "installments": installments,
    "link": link,
  };
}