
import 'package:testing_list_feature/freightage/models/freightage_models.dart/tags_model.dart';

import 'invoice_model.dart';

class OptionsModel {
  int insuranceValue;
  bool receipt;
  bool ownHand;
  bool reverse;
  bool nonCommercial;
  InvoiceModel invoice;
  String platform;
  List<TagsModel> tags;

  OptionsModel(
      {this.insuranceValue,
      this.receipt,
      this.ownHand,
      this.reverse,
      this.nonCommercial,
      this.invoice,
      this.platform,
      this.tags});

  OptionsModel.fromJson(Map<String, dynamic> json) {
    insuranceValue = json['insurance_value'];
    receipt = json['receipt'];
    ownHand = json['own_hand'];
    reverse = json['reverse'];
    nonCommercial = json['non_commercial'];
    invoice =
        json['invoice'] != null ? new InvoiceModel.fromJson(json['invoice']) : null;
    platform = json['platform'];
    if (json['tags'] != null) {
      tags = new List<TagsModel>();
      json['tags'].forEach((v) {
        tags.add(new TagsModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['insurance_value'] = this.insuranceValue;
    data['receipt'] = this.receipt;
    data['own_hand'] = this.ownHand;
    data['reverse'] = this.reverse;
    data['non_commercial'] = this.nonCommercial;
    if (this.invoice != null) {
      data['invoice'] = this.invoice.toJson();
    }
    data['platform'] = this.platform;
    if (this.tags != null) {
      data['tags'] = this.tags.map((v) => v.toJson()).toList();
    }
    return data;
  }
}