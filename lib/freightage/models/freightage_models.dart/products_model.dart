
import 'package:testing_list_feature/freightage/models/order_models.dart/order_line_item_model.dart';

class ProductsModel {
  String name;
  int quantity;
  int unitaryValue;

  ProductsModel({this.name, this.quantity, this.unitaryValue});

  ProductsModel.fromLineItems(OrderLineItem lineItem){
    this.name = lineItem.name;
    this.quantity = lineItem.quantity;
    this.unitaryValue = lineItem.price.toInt() * 100;
  }

  ProductsModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    quantity = json['quantity'];
    unitaryValue = json['unitary_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['unitary_value'] = this.unitaryValue;
    return data;
  }
}