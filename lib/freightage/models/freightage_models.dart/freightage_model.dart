import 'package:testing_list_feature/freightage/models/order_models.dart/order_model.dart';

import 'options_model.dart';
import 'products_model.dart';
import 'receiver_model.dart';
import 'sender_model.dart';
import 'tags_model.dart';
import 'volumes_model.dart';

class FreightageModel {
  int service;
  int agency;
  Sender from;
  Receiver to;
  List<ProductsModel> products;
  List<VolumesModel> volumes;
  OptionsModel options;

  FreightageModel(
      {this.service,
      this.agency,
      this.from,
      this.to,
      this.products,
      this.volumes,
      this.options});

  FreightageModel.fillReceiver(Order order) {
    final String fullName =
        '${order.shipping.firstName} ${order.shipping.lastName}';
    final String fullAddress =
        '${order.shipping.address1} ${order.shipping.address2}';
        
    this.to = Receiver(
        address: fullAddress,
        number: order.shipping.number,
        city: order.shipping.city,
        countryId: order.shipping.country,
        district: order.shipping.neighborhood,
        stateAbbr: order.shipping.state,
        postalCode: order.shipping.postcode,
        name: fullName,
        email: order.billing.email,
        phone: order.billing.phone,
        companyDocument: order.billing.cnpj,
        document: order.billing.cpf);
    this.options = OptionsModel(tags: [TagsModel(tag: order.orderKey, url: '')]);
    this.products = order.lineItems.map((e) => ProductsModel.fromLineItems(e)).toList();
  }

  FreightageModel.fromJson(Map<String, dynamic> json) {
    service = json['service'];
    agency = json['agency'];
    from = json['from'] != null ? new Sender.fromJson(json['from']) : null;
    to = json['to'] != null ? new Receiver.fromJson(json['to']) : null;
    if (json['products'] != null) {
      products = new List<ProductsModel>();
      json['products'].forEach((v) {
        products.add(new ProductsModel.fromJson(v));
      });
    }
    if (json['volumes'] != null) {
      volumes = new List<VolumesModel>();
      json['volumes'].forEach((v) {
        volumes.add(new VolumesModel.fromJson(v));
      });
    }
    options = json['options'] != null
        ? new OptionsModel.fromJson(json['options'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['service'] = this.service;
    data['agency'] = this.agency;
    if (this.from != null) {
      data['from'] = this.from.toJson();
    }
    if (this.to != null) {
      data['to'] = this.to.toJson();
    }
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    if (this.volumes != null) {
      data['volumes'] = this.volumes.map((v) => v.toJson()).toList();
    }
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    return data;
  }
}
