
class Receiver {
  String name;
  String phone;
  String email;
  String document;
  String companyDocument;
  String stateRegister;
  String address;
  String complement;
  String number;
  String district;
  String city;
  String stateAbbr;
  String countryId;
  String postalCode;
  String note;

  Receiver(
      {this.name,
      this.phone,
      this.email,
      this.document,
      this.companyDocument,
      this.stateRegister,
      this.address,
      this.complement,
      this.number,
      this.district,
      this.city,
      this.stateAbbr,
      this.countryId,
      this.postalCode,
      this.note});

  Receiver.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    phone = json['phone'];
    email = json['email'];
    document = json['document'];
    companyDocument = json['company_document'];
    stateRegister = json['state_register'];
    address = json['address'];
    complement = json['complement'];
    number = json['number'];
    district = json['district'];
    city = json['city'];
    stateAbbr = json['state_abbr'];
    countryId = json['country_id'];
    postalCode = json['postal_code'];
    note = json['note'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['document'] = this.document;
    data['company_document'] = this.companyDocument;
    data['state_register'] = this.stateRegister;
    data['address'] = this.address;
    data['complement'] = this.complement;
    data['number'] = this.number;
    data['district'] = this.district;
    data['city'] = this.city;
    data['state_abbr'] = this.stateAbbr;
    data['country_id'] = this.countryId;
    data['postal_code'] = this.postalCode;
    data['note'] = this.note;
    return data;
  }
}