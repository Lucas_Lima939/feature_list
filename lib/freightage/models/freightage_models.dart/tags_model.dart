

class TagsModel {
  String tag;
  String url;

  TagsModel({this.tag, this.url});

  TagsModel.fromJson(Map<String, dynamic> json) {
    tag = json['tag'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tag'] = this.tag;
    data['url'] = this.url;
    return data;
  }
}