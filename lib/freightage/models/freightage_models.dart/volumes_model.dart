
class VolumesModel {
  int height;
  int width;
  int length;
  int weight;

  VolumesModel({this.height, this.width, this.length, this.weight});

  VolumesModel.fromJson(Map<String, dynamic> json) {
    height = json['height'];
    width = json['width'];
    length = json['length'];
    weight = json['weight'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['height'] = this.height;
    data['width'] = this.width;
    data['length'] = this.length;
    data['weight'] = this.weight;
    return data;
  }
}