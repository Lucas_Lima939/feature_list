import 'package:flutter/material.dart';
import 'package:testing_list_feature/freightage/models/agency_model.dart/agency_model.dart';
import 'package:testing_list_feature/list_products_wc/widgets/likebag-util.dart';
import 'package:testing_list_feature/list_products_wc/widgets/likebag_theme_app.dart';


class CusGridTile extends StatelessWidget {
  final AgencyModel agency;
  final Function onTap;

  const CusGridTile(
      {Key key, @required this.onTap, @required this.agency})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: LikeBagThemeApp.blue,
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),
          //color: getListTileColor(context,snapshot.data[index]["name"]),
          child: GridTile(
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: Text(
                  agency.name,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ),
      ),
      onTap: onTap,
    );
  }
}
