import 'package:flutter/material.dart';
import 'package:testing_list_feature/freightage/choose_agency_page.dart';
import 'package:testing_list_feature/list_products_wc/widgets/likebag_theme_app.dart';
import 'package:testing_list_feature/list_products_wc/widgets/nav.dart';
import 'package:testing_list_feature/list_products_wc/woocommerce_service.dart';

import '../list_products_wc/widgets/likebag_text_form_filed.dart';
import 'freightage_bloc.dart';
import 'models/order_models.dart/order_model.dart';

class FreightageForm extends StatefulWidget {
  @override
  _FreightageFormState createState() => _FreightageFormState();
}

class _FreightageFormState extends State<FreightageForm> {
  FreightageBloc freightageBloc;
  WoocommerceService woocommerceService;

  @override
  void initState() {
    freightageBloc = FreightageBloc();
    freightageBloc.finishOrder();
    freightageBloc.listOrder();
    
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Lista de Pedidos'),
        backgroundColor:  LikeBagThemeApp.darkBlue,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(
            horizontal: 10
        ) ,
        child: StreamBuilder<List<Order>>(
          stream: freightageBloc.outOrders,
          builder: (context, snapshot) {
            if(!snapshot.hasData)
            return Center(child: CircularProgressIndicator());
            else
            return ListView(
              children: snapshot.data.map((order) => Card(
                color:  LikeBagThemeApp.darkBlue,
                child: ListTile(leading: Text(order.number, style: TextStyle(color: Colors.white),), title: Text(order.status,  style: TextStyle(color: Colors.white),), onTap: (){},))).toList()
            );
          }
        ),
      ),
      bottomNavigationBar: BottomAppBar(
            color: LikeBagThemeApp.darkBlue,
            child: Container(
                height: 50.0,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        child: Text('AVANÇAR', style: TextStyle(color: Colors.white),),
                        onPressed: () {
                          push(context, ChooseAgencyPage());
                        },
                      ),
                    ])))   
    );

  }

  @override
  void dispose() {
    super.dispose();
  }
}