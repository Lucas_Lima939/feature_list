import 'dart:convert';

import 'package:http/http.dart' as http;
import 'models/agency_model.dart/agency_model.dart';
import 'models/order_models.dart/order_model.dart';

class MelhorEnvioRepo {
  Future<void> finishOrder() async {
    try {
      final http.Response res = await http.post(url('cart'));
      if (res.statusCode == 200) {
        final List jsonList = jsonDecode(res.body);
      } else {
        return;
      }
    } catch (e) {
      print(e);
      return;
    }
  }

  Future<void> listServices() async {
    try {
      final http.Response res = await http.get(url('shipment/services'));
      if (res.statusCode == 200) {
        final List jsonList = jsonDecode(res.body);
      } else {
        return;
      }
    } catch (e) {
      print(e);
      return;
    }
  }

  Future<void> listAppInfos() async {
    try {
      final http.Response res = await http.get(url('shipment/app-settings'));
      if (res.statusCode == 200) {
        final List jsonList = jsonDecode(res.body);
      } else {
        return;
      }
    } catch (e) {
      print(e);
      return;
    }
  }

  Future<List<AgencyModel>> listAvaliableAgencies(String stateAbbreviation) async {
    try {
      final List<AgencyModel> agencies = [];
      final http.Response res =
          await http.get(url('shipment/agencies?state=$stateAbbreviation'));
      if (res.statusCode == 200) {
        jsonDecode(res.body).forEach((x)=> agencies.add(AgencyModel.fromJson(x)));
        return agencies;
      } else {
        return agencies;
      }
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<Order>> getOrders() async {
    try {
      final http.Response res = await http.get(wpUrl('orders?'));
      if (res.statusCode == 200) {
        final List jsonList = jsonDecode(res.body);
        final List<Order> orders =
            jsonList.map((json) => Order.fromJson(json)).toList();
        return orders;
      } else {
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }
  }
}

const String BASE_URL = 'https://www.melhorenvio.com.br/api/v2/me/';
const String TOKEN =
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImViYmEzNDc2OWZmMmNmNmEzNzc4ODEyOWI0Y2NjMGFmNDRhOGJlNWQzMGRjZDBhMWFjMDVmOGVmZDI5OTQ4ZTE0YTdhODUyNGIyMmU0YWZjIn0.eyJhdWQiOiIxIiwianRpIjoiZWJiYTM0NzY5ZmYyY2Y2YTM3Nzg4MTI5YjRjY2MwYWY0NGE4YmU1ZDMwZGNkMGExYWMwNWY4ZWZkMjk5NDhlMTRhN2E4NTI0YjIyZTRhZmMiLCJpYXQiOjE1OTcwNzE3NDgsIm5iZiI6MTU5NzA3MTc0OCwiZXhwIjoxNjI4NjA3NzQ4LCJzdWIiOiI4ZjZhYTc3NC1iMjgyLTQ5NzktODA2Zi1iOWI3ZmVjMjQwNTciLCJzY29wZXMiOlsiY2FydC1yZWFkIiwiY2FydC13cml0ZSIsImNvbXBhbmllcy1yZWFkIiwiY29tcGFuaWVzLXdyaXRlIiwiY291cG9ucy1yZWFkIiwiY291cG9ucy13cml0ZSIsIm5vdGlmaWNhdGlvbnMtcmVhZCIsIm9yZGVycy1yZWFkIiwicHJvZHVjdHMtcmVhZCIsInByb2R1Y3RzLXdyaXRlIiwicHVyY2hhc2VzLXJlYWQiLCJzaGlwcGluZy1jYWxjdWxhdGUiLCJzaGlwcGluZy1jYW5jZWwiLCJzaGlwcGluZy1jaGVja291dCIsInNoaXBwaW5nLWNvbXBhbmllcyIsInNoaXBwaW5nLWdlbmVyYXRlIiwic2hpcHBpbmctcHJldmlldyIsInNoaXBwaW5nLXByaW50Iiwic2hpcHBpbmctc2hhcmUiLCJzaGlwcGluZy10cmFja2luZyIsImVjb21tZXJjZS1zaGlwcGluZyIsInRyYW5zYWN0aW9ucy1yZWFkIiwidXNlcnMtcmVhZCIsInVzZXJzLXdyaXRlIiwid2ViaG9va3MtcmVhZCIsIndlYmhvb2tzLXdyaXRlIiwibWluaGFzdmVuZGFzOjpjbGllbnRzOnJlYWQiLCJtaW5oYXN2ZW5kYXM6OmNsaWVudHM6d3JpdGUiLCJtaW5oYXN2ZW5kYXM6OnNhbGVzOnJlYWQiLCJtaW5oYXN2ZW5kYXM6OnNhbGVzOndyaXRlIiwibWluaGFzdmVuZGFzOjp3ZWJob29rczpyZWFkIiwibWluaGFzdmVuZGFzOjp3ZWJob29rczp3cml0ZSIsIm1pbmhhc3ZlbmRhczo6c2hpcHBpbmdzOnJlYWQiLCJtaW5oYXN2ZW5kYXM6OnNoaXBwaW5nczp3cml0ZSIsIm1pbmhhc3ZlbmRhczo6Y29uY2lsaWF0aW9uczpyZWFkIiwibWluaGFzdmVuZGFzOjpjb25jaWxpYXRpb25zOndyaXRlIl19.0LPdFYKrQHMowiQKtIZiofA2o3mDY3gopRorEeZpki2lk4xC_rQ-j0SnnaYqs442nLKlFGSEABuXdOQJcCTX4eqabj2UGR3baPKXzXI3fMfdmEF327YGXqPiVwSQOUYy2cedndD6GOHNAMCQpTW7MXzuSa-nL3CxOzQ-cGpVyuTWIoRU_FcP6yidDvjzGLo2wBze47Q11AW0dYph0rsrs-sswEVLZWLF2mllB-3ZtzXNZLjLt68Z87LSGAROp9bx3PbZUFaenKOtp9wN_K-f4TW_3cXMmXTQ79h-v7pAmg2PBsIP1s2KpYQxEQyQeurH4GAz_4p1t6riGXp3Cv83ImprTRO_vJ70RnWii9uVUe-xHmvI5eQ5zIAHw9ZxgMncbh3d7GKAkOO3RUXrcZWrM9B4OLtHdOgMSRVjU4pS2VLXC7THNt7RTsGSEbTefPnpyZaSzhq4I5Gy0QS2rmRCaVwSZtlLd0TsBGfKJy4l6eIMk0EEiNaMPAiPlIY_h9Q-3qt-myigAdjK-a1yTPwzGPtDxm2iWC109HWyXXkbzfRGyh0Ckrh_RfuFzmAb3DblM2gLV8MrLsoOLL0Xj-7SmK2p85w4rrb2-3lRlGnlHXfHnK14zNWsWwkEk_AG8u52c2fySFu7pP5jfmaQdo2KL1u0tqIC79I6PMlet8VMyxY';

String url(String endpoint) {
  return BASE_URL + endpoint;
}

final String WP_BASE_URL = 'https://devapp.likebag.com.br/wp-json/wc/v3/';
final String CONSUMER_KEY_SECRET =
    'consumer_key=ck_dbfc4869adeaea59593cd82dc3b004f206fb7589&consumer_secret=cs_b8dfa76f6b1c22c3c369b294b429ba6ed68457f8';

String wpUrl(String endpoint) {
  return WP_BASE_URL + endpoint + CONSUMER_KEY_SECRET;
}
