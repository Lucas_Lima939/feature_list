import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:testing_list_feature/freightage/models/agency_model.dart/agency_model.dart';
import 'package:testing_list_feature/list_products_wc/woocommerce_service.dart';

import 'melhor_envio_repo.dart';
import 'models/freightage_models.dart/freightage_model.dart';
import 'models/order_models.dart/order_model.dart';

class FreightageBloc extends BlocBase {
  final _clientNameController = BehaviorSubject<String>();
  final _clientPhoneController = BehaviorSubject<String>();
  final _clientEmailController = BehaviorSubject<String>();
  final _clientCpfController = BehaviorSubject<String>();
  final _clientCnpjController = BehaviorSubject<String>();
  final _clientAddressController = BehaviorSubject<String>();
  final _clientComplementController = BehaviorSubject<String>();
  final _clientHouseNumberController = BehaviorSubject<String>();
  final _clientDistrictController = BehaviorSubject<String>();
  final _clientCityController = BehaviorSubject<String>();
  final _clientStateAbbreviationController = BehaviorSubject<String>();
  final _clientCountryAbbreviationController = BehaviorSubject<String>();
  final _clientPostalCodeController = BehaviorSubject<String>();
  final _clientFinalNotesController = BehaviorSubject<String>();

  final _processingOrdersController = BehaviorSubject<List<Order>>();
  final _agenciesController = BehaviorSubject<List<AgencyModel>>();

  Stream get outClientName => _clientNameController.stream;
  Stream get outClientPhone => _clientPhoneController.stream;
  Stream get outClientEmail => _clientEmailController.stream;
  Stream get outClientCpf => _clientCpfController.stream;
  Stream get outClientCnpj => _clientCnpjController.stream;
  Stream get outClientAddress => _clientAddressController.stream;
  Stream get outClientComplement => _clientComplementController.stream;
  Stream get outClientHouseNumber => _clientHouseNumberController.stream;
  Stream get outClientDistrict => _clientDistrictController.stream;
  Stream get outClientCity => _clientCityController.stream;
  Stream get outClientStateAbbreviation =>
      _clientStateAbbreviationController.stream;
  Stream get outClientCountryAbbreviation =>
      _clientCountryAbbreviationController.stream;
  Stream get outClientPostalCode => _clientPostalCodeController.stream;
  Stream get outClientFinalNotes => _clientFinalNotesController.stream;
  Stream get outOrders => _processingOrdersController.stream;
  Stream get outAgencies => _agenciesController.stream;

  Function(String) get changeClientName => _clientNameController.sink.add;
  Function(String) get changeClientPhone => _clientPhoneController.sink.add;
  Function(String) get changeClientEmail => _clientEmailController.sink.add;
  Function(String) get changeClientCpf => _clientCpfController.sink.add;
  Function(String) get changeClientCnpj => _clientCnpjController.sink.add;
  Function(String) get changeClientAddress => _clientAddressController.sink.add;
  Function(String) get changeClientComplement =>
      _clientComplementController.sink.add;
  Function(String) get changeClientHouseNumber =>
      _clientHouseNumberController.sink.add;
  Function(String) get changeClientDistrict =>
      _clientDistrictController.sink.add;
  Function(String) get changeClientCity => _clientCityController.sink.add;
  Function(String) get changeClientStateAbbreviation =>
      _clientStateAbbreviationController.sink.add;
  Function(String) get changeClientCountryAbbreviation =>
      _clientCountryAbbreviationController.sink.add;
  Function(String) get changeClientPostalCode =>
      _clientPostalCodeController.sink.add;
  Function(String) get changeClientFinalNotes =>
      _clientFinalNotesController.sink.add;

  final meRepo = MelhorEnvioRepo();
  final wpService = WoocommerceService();

  finishOrder() async {
    final Order order = await wpService.getOrders();
    final FreightageModel freightage = FreightageModel.fillReceiver(order);
    print(freightage);
    // await repo.finishOrder();
  }

  Future<void> listAgencies() async {
    await meRepo.listAvaliableAgencies('PE').then((agencies) => 
    _agenciesController.add(agencies)
    );
  }

  Future<void> listOrder() async {
    List<Order> _orders = [];
    int _page = 1;
    bool hasMore = true;
    while (hasMore) {
      await wpService.getOrdersProcessing(_page).then((response) {
        hasMore = response.length == 100;
        _orders.addAll(response);
        _page++;
      });
    }
    _processingOrdersController.add(_orders);
  }

  @override
  void dispose() {
    _clientNameController.close();
    _clientPhoneController.close();
    _clientEmailController.close();
    _clientCpfController.close();
    _clientCnpjController.close();
    _clientAddressController.close();
    _clientComplementController.close();
    _clientHouseNumberController.close();
    _clientDistrictController.close();
    _clientCityController.close();
    _clientStateAbbreviationController.close();
    _clientCountryAbbreviationController.close();
    _clientPostalCodeController.close();
    _clientFinalNotesController.close();
    _processingOrdersController.close();
    _agenciesController.close();
    super.dispose();
  }
}
