import 'package:flutter/material.dart';
import 'freightage/freightage_form.dart';
import 'list_products_wc/data_search.dart';
import 'list_products_wc/filtered_list.dart';
import 'list_products_wc/widgets/nav.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      theme: ThemeData(
          primarySwatch: Colors.blue,
          brightness: Brightness.light,
          scaffoldBackgroundColor: Colors.white),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                showSearch(context: context, delegate: DataSearch(), );
              } //async => await push(context, ProductsList()),
              )
        ],
      ),
      body: Center(child: GestureDetector(child: Text('Lista de Produtos'), onTap: () => push(context, FreightageForm()),)),
    );
  }
}
